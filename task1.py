"""
Write a code using pytorch to replicate a grouped 2D convolution layer based on the original 2D convolution. 

The common way of using grouped 2D convolution layer in Pytorch is to use 
torch.nn.Conv2d(groups=n), where n is the number of groups.

However, it is possible to use a stack of n torch.nn.Conv2d(groups=1) to replicate the same
result. The wights must be copied and be split between the convs in the stack.

You can use:
    - use default values for anything unspecified
    - all available functions in NumPy and Pytorch
    - the custom layer must be able to take all parameters of the original nn.Conv2d
"""

import numpy as np
import torch
import torch.nn as nn


torch.manual_seed(8)    # DO NOT MODIFY!
np.random.seed(8)   # DO NOT MODIFY!


#define the random input parameters and hyperparameters for the CNN  (values currently the same as given in the task, but can be changed)
batch = 2 #number of inputs processed in a batch
in_channels = 64 #number of channels in one torch tensor
height = 100 #height of torch tensor
width = 100 #width of torch tensor
n = 16 #number of groups
out_channels = 128 #number of filters/features

# random input (batch, channels, height, width)
x = torch.randn(batch, in_channels, height, width) #define the torch tensor



#CHECK IF THE NUMBER OF GROUPS IS DIVISIBLE BY INPUT AND INPUT CHANNELS
if (in_channels%n==0) and (out_channels%n==0):

    # ORIGINAL 2D CONVOLUTION
    grouped_layer = nn.Conv2d(in_channels, out_channels, 3, stride=1, padding=0, groups=n, bias=True)

    # weights and bias are extracted
    w_torch = grouped_layer.weight
    b_torch = grouped_layer.bias

    #the final convoluted layer
    y_1 = grouped_layer(x)

    #SAME PROCESS THROUGH A CUSTOMISED CLASS
    class CustomGroupedConv2D(nn.Module):
        def __init__(self, in_channels, out_channels, n_g, weights, biases):
            super().__init__()
            self.input_channel = in_channels
            self.output_channel = out_channels
            self.groups = n_g
            self.weights = weights
            self.biases = biases

        def forward(self, x):
            # INPUT TENSOR IS SPLIT ACCORDING TO NUMBER OF GROUPS
            input = torch.split(x, int(self.input_channel / self.groups), dim=1)
            # WEIGHT TENSOR IS SPLIT
            weight_each_group = torch.split(self.weights, int(self.output_channel / self.groups), dim=0)
            #BIAS TENSOR IS SPLIT
            biases_each_group = torch.split(self.biases, int(self.output_channel / self.groups), dim=0)
            output = []
            #EACH COMBINATION IS PROCESSED SEPARATELY THROUGH nn.functional.conv2d WITH groups=1
            for i in range(0, len(input)):
                intermediate_output = nn.functional.conv2d(input[i], weight_each_group[i], biases_each_group[i], stride=1, padding=0, groups=1)
                output.append(intermediate_output)
            #ALL OUTPUT CHANNELS ARE THEN CONCATENATED TO GET A SINGLE TENSOR
            output = torch.cat(output, dim=1)
            return output


    #inputs are input channel, output channel, n=number of groups, w_torch=weight tensor generated from nn.Conv2d, b_torch=bias tensor generated from nn.Conv2d
    custom_convolution = CustomGroupedConv2D(in_channels, out_channels, n, w_torch, b_torch)
    y_2 = custom_convolution.forward(x)

    #THERE ARE DIFFERENT WAY TO COMPARE THE TWO TENSORS, AND EACH COMPARISON COULD BRING DIFFERENT RESULTS

    #1. SHAPE OF BOTH THE TENSORS SHOULD BE SAME
    print("1 (a). Size of convoluted layer from direct implementation", y_1.shape)
    print("1 (b). Size of convoluted layer output from CustomGroupedConv2D(x)", y_2.shape)

    #2. USE ALLCLOSE FUNCTION FROM PYTORCH, ONE AMBIGUITY HERE IS THAT IT MIGHT RETURN TRUE OR FALSE VALUES FOR DIFFERENT TOLERANCE VALUES
    #FOR THE GIVEN TENSOR PARAMETERS IN THE TASK, THE FUNCTION RETURNS TRUE FOR rtol=1e-03, atol=1e-06; BUT COULD RETURN FALSE IF THE TENSOR PARAMETERS ARE CHANGED
    print("2. Comparison of two tensors using torch.allclose", torch.allclose(y_1, y_2, rtol=1e-03, atol=1e-06)) #This will return True value if the error between the two sensors is less than a certain value calculated from the tolerance limit that has been initialized

    #3. THE DISTANCE BETWEEN THE TWO TENSORS (p-Norm=2); A VERY SMALL VALUE MEANS THE TENSORS ARE NEARER
    print("3. The distance between the two different outputs (p-Norm=2) is", torch.dist(y_1, y_2, p=2))

else:
    print("The number of groups is not divisible by input and output channels")