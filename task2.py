"""
Use the following augmentation methods on the sample image under data/sample.png
and save the result under this path: 'data/sample_augmented.png'

Note:
    - use torchvision.transforms
    - use the following augmentation methods with the same order as below:
        * affine: degrees: ±5, 
                  translation= 0.1 of width and height, 
                  scale: 0.9-1.1 of the original size
        * rotation ±5 degrees,
        * horizontal flip with a probablity of 0.5
        * center crop with height=320 and width=640
        * resize to height=160 and width=320
        * color jitter with:  brightness=0.5, 
                              contrast=0.5, 
                              saturation=0.4, 
                              hue=0.2
    - use default values for anything unspecified
"""

import torch
from torchvision import transforms as T
from torchvision.io import read_image
import numpy as np
import cv2


torch.manual_seed(8)
np.random.seed(8)

# write your code here ...
#READ THE IMAGE IN TORCH TENSOR FORM OF SIZE [image_channels, image_height, image_width]
img = read_image('data/sample.png')
# display the image properties
print("Image data:", img)

# check if input image is a PyTorch tensor
print("Is image a PyTorch Tensor:", torch.is_tensor(img))
print("Type of Image:", type(img))


#CREATE TORCHVISION TRANSFORM PIPELINE WITH TRANSFORMATIONS AND VALUES GIVEN
transform_image = T.Compose([
                  T.RandomAffine(degrees = 5, translate= (0.1, 0.1), scale= (0.9, 1.1)),
                  T.RandomRotation(degrees=5),
                  T.RandomHorizontalFlip(p=0.5),
                  T.CenterCrop((320, 640)),
                  T.Resize((160, 320)),
                  T.ColorJitter(brightness=0.5, contrast=0.5, saturation=0.4, hue=0.2)])

img_augmented = transform_image(img) #do the transformation

img_augmented = T.ToPILImage()(img_augmented) #convert to PIL
img_augmented.save("data/sample_augmented.png","PNG") #save as a PNG file in the given folder

img_augmented.show() #to show the image

