"""
develop a model based on the onnx file in model/model.onnx 

Note:
    - initialize the convolutions layer with uniform xavier
    - initialize the linear layer with a normal distribution (mean=0.0, std=1.0)
    - initialize all biases with zeros
    - use batch norm wherever is relevant
    - use random seed 8
    - use default values for anything unspecified
"""

import numpy as np
import torch
import torch.nn as nn
import tkinter as tk
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import askdirectory



torch.manual_seed(8)    # DO NOT MODIFY!
np.random.seed(8)   # DO NOT MODIFY!


# write your code here ...

import onnx
from onnx import numpy_helper


# Define the uniform Xavier initialization function
def xavier_uniform_initialization(shape):
    input_of_layer, output_of_layer = shape[1], shape[0]
    range = np.sqrt(6 / (input_of_layer + output_of_layer))
    return np.random.uniform(-range, range, size=shape)

#LOAD THE GIVEN MODEL
model = onnx.load("model/model.onnx")

# model is an onnx model
onnx.checker.check_model(model)
initializers=model.graph.initializer

#ITERATE OVER DIFFERENT INITIALIZERS TO MODIFY THE INITIALZERS AS REQUESTED
for initializer in initializers:
    #to initialize conv weights with uniform xavier
    if initializer.name.endswith("conv.weight"):
        shape = initializer.dims
        weight_xavier = xavier_uniform_initialization(shape)
        initializer.CopyFrom(onnx.numpy_helper.from_array(weight_xavier, initializer.name))
    #to initialize linear layer weights with a normal distribution with mean=0, and STD_DEV=1
    if initializer.name == "linear.weight":
        shape = initializer.dims
        weight_normals = np.random.normal(0, 1.0, shape)
        initializer.CopyFrom(onnx.numpy_helper.from_array(weight_normals, initializer.name))
    #to initialize all the biases with zero
    if initializer.name.endswith("bias"):
        shape = initializer.dims
        weight_new = np.zeros(shape)
        initializer.CopyFrom(onnx.numpy_helper.from_array(weight_new, initializer.name))


#TO ADD BATCH NORMALIZATION NODES, I DID HALF OF THE WORK BY FINDING THE NUMBER OF INPUT AND OUTPUT CHANNELS FOR EACH CONV NODE
for node in model.graph.node:
    op_type = node.op_type
    inputs = node.input
    outputs = node.output
    attributes = node.attribute
    if op_type=="Conv":
        # Get the input and output names of the Conv node
        input_name = inputs[1]
        for tensor in model.graph.initializer:
            if tensor.name == input_name:
                input_shape = tensor.dims

        # Get the number of input channels of the Conv node
        num_ip_channels = input_shape[1]

        # Get the number of output channels of the Conv node
        num_op_channels = input_shape[0]

        print("Node: op_type={}, inputs={}, outputs={}, attributes={}".format(op_type, inputs, outputs, attributes))
        print('Conv node {} has {} input channels and {} output channels'.format(node.name, num_ip_channels, num_op_channels))

#A FUNCTION CAN BE CALLED TO ADD A BATCHNORM NODE AFTER EVERY CONV LAYER

#SAVE THE MODEL WITH THE UPDATED INITIALIZERS IN PATH "model/model_updated.onnx"
onnx.checker.check_model(model)
model_path = "model/model_updated.onnx"
onnx.save(model, model_path)


